// Task Implement a struct named 'RangeList'
// A pair of integers define a range, for example [1, 5). This range includes integers 1, 2, 3, and 4.
// A range list is an aggregate of these ranges [1, 5), [10, 11), [100, 201)
// NOTE Feel free to add any extra member variables/functions you like.
package main

import (
	"fmt"
	"strconv"
)

const (
	HeadNode   int = -1
	NormalNode int = 1
)

type RangeListNode struct {
	//区间左值
	left int
	//区间右值
	right int
	//节点类型
	nodeType int
	//后继节点
	next *RangeListNode
	//前驱节点
	pre *RangeListNode
}

func CreateHeadNode() *RangeListNode {
	return &RangeListNode{nodeType: HeadNode}
}
func CreateNormalNode(left int, right int) *RangeListNode {
	return &RangeListNode{left: left, right: right, nodeType: NormalNode}
}
func (rangeListNode *RangeListNode) ToString() string {
	if rangeListNode.IsHeader() {
		return ""
	}
	return "[" + strconv.Itoa(rangeListNode.left) + "," + strconv.Itoa(rangeListNode.right) + ")"
}

func (rangeListNode *RangeListNode) IsHeader() bool {
	return rangeListNode.nodeType == HeadNode
}

type RangeList struct {
	//头节点
	header *RangeListNode
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	if rangeList.header == nil {
		rangeList.header = CreateHeadNode()
	}
	var needInsertNode = CreateNormalNode(rangeElement[0], rangeElement[1])
	var preNode = rangeList.findNodeBeforeOrContainTheNum(needInsertNode.left)
	rangeList.insertNode(preNode, needInsertNode)
	rangeList.mergeNode(preNode)
	return nil
}

func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	// 找到需要操作的左右边界节点
	var leftNode = rangeList.findNodeBeforeOrContainTheNum(rangeElement[0])
	var rightNode = rangeList.findNodeBeforeOrContainTheNum(rangeElement[1])
	//删除中间的其他节点
	if leftNode != rightNode {
		leftNode.next = rightNode
		rightNode.pre = leftNode
	}
	//这两个节点进行切分或者删除操作
	rangeList.splitNodeWithRange(leftNode, rangeElement[0], rangeElement[1])
	rangeList.splitNodeWithRange(rightNode, rangeElement[0], rangeElement[1])
	return nil
}

func (rangeList *RangeList) Print() error {
	var text = ""
	var node = rangeList.header
	for {
		if node == nil {
			break
		}
		text += node.ToString()
		node = node.next
	}
	fmt.Println(text)
	return nil
}

// 找到目标数字在链表中的前一个结点
func (rangeList *RangeList) findNodeBeforeOrContainTheNum(num int) *RangeListNode {
	var curNode = rangeList.header
	for {
		if curNode == nil || curNode.next == nil || curNode.next.left >= num {
			break
		}
		curNode = curNode.next
	}
	return curNode
}

// 插入节点
func (rangeList *RangeList) insertNode(preNode *RangeListNode, needInsertNode *RangeListNode) {
	needInsertNode.next = preNode.next
	preNode.next = needInsertNode
	if preNode.next != nil {
		needInsertNode.pre = preNode.next.pre
		preNode.next.pre = needInsertNode
	}
}

// 合并重叠区间
func (rangeList *RangeList) mergeNode(startNode *RangeListNode) {

	//头节点不处理
	if startNode.IsHeader() {
		startNode = startNode.next
	}

	var lastNeedMergeNode = startNode
	var rightest = lastNeedMergeNode.right
	//一直扩展到被区间覆盖的最右的节点
	for {
		if lastNeedMergeNode.next == nil || rightest < lastNeedMergeNode.next.left {
			break
		}
		rightest = max(lastNeedMergeNode.next.right, rightest)
		lastNeedMergeNode = lastNeedMergeNode.next
	}

	//中间的全删
	startNode.right = rightest
	startNode.next = lastNeedMergeNode.next
	if startNode.next != nil {
		startNode.next.pre = startNode
	}
}

// 删除节点
func (rangeList *RangeList) removeNode(needRemoveNode *RangeListNode) {
	if needRemoveNode.IsHeader() {
		return
	}
	if needRemoveNode.pre == nil {
		return
	}
	needRemoveNode.pre.next = needRemoveNode.next
	if needRemoveNode.next != nil {
		needRemoveNode.next.pre = needRemoveNode.pre
	}
}

// 从节点中删去指定的区间
func (rangeList *RangeList) splitNodeWithRange(needSplitNode *RangeListNode, left int, right int) {
	//头节点直接放过
	if needSplitNode.IsHeader() {
		return
	}
	//没有重叠的部分，无需处理
	if right < needSplitNode.left || needSplitNode.right < left {
		return
	}
	//节点需要整个删除
	if left <= needSplitNode.left && needSplitNode.right <= right {
		rangeList.removeNode(needSplitNode)
		return
	}
	//只删除左半区
	if left <= needSplitNode.left {
		needSplitNode.left = right
		return
	}
	//只删除右半区
	if needSplitNode.right <= right {
		needSplitNode.right = left
		return
	}
	//一分为二
	var leftNode = needSplitNode
	var rightNode = CreateNormalNode(right, leftNode.right)
	leftNode.right = left
	rangeList.insertNode(leftNode, rightNode)
}

func max(a, b int) int {
	if a < b {
		return b
	}
	return a
}

func main() {
	fmt.Println(CreateNormalNode(1, 2).ToString())

	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print()
	// Should display [1, 3) [19, 21)
	rl.Add([2]int{-100, 100})
	rl.Print()
	// Should display [-100, 100)
	rl.Remove([2]int{-1, 119})
	rl.Print()
	// Should display [-100, -1)
}
